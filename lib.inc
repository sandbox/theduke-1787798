<?php
/**
 * @file
 *   Contains classes for writing to logstash.
 */

interface Logstash_Writer {

  /**
   * Do any initialization that is neccessary.
   *
   * @param string $host
   * @param integer $port
   * @throws Exception
   */
  public function __construct($host, $port);

  /**
   * Do neccessary teardown.
   */
  public function __destruct();

  /**
   * Write a single log message to Logstash.
   *
   * @param  array  $data
   * @throws Exception
   */
  public function writeMessage(array $data);
}

abstract class Logstash_Writer_SocketStream implements Logstash_Writer {
  protected $protocol = '';

  /**
   * @var integer
   */
  protected $socket;

  public function __construct($host, $port) {
    $this->socket = fsockopen($this->protocol . '://' . $host, $port, $errno, $errstr);

    if (!$this->socket) {
      throw new Exception('Could not open UDP socket for logstash: ' . $errstr);
    }
  }

  public function __destruct() {
    fclose($this->socket);
  }

  public function writeMessage(array $data) {
    $msg = json_encode($data) . "\n";

    if (!fwrite($this->socket, $msg)) {

      throw new Exception('Could not send message to Logstash server: ' . $err);
    }
  }
}

class Logstash_Writer_UDP extends Logstash_Writer_SocketStream {
  protected $protocol = 'udp';
}

class Logstash_Writer_TCP extends Logstash_Writer_SocketStream {
  protected $protocol = 'tcp';
}

class Logstash_Writer_DrupalQueue implements Logstash_Writer {
  /**
   * @var DrupalQueue
   */
  protected $queue;

  public function __construct($host, $port) {
    $this->queue = DrupalQueue::get('logstash', TRUE);
  }

  public function __destruct() {
    // Nothing to do here.
  }

  public function writeMessage(array $data) {
    $this->queue->createItem($data);
  }
}
